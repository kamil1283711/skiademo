import math
import numpy as np
import skia
import tkinter as tk
from PIL import Image, ImageTk
import scipy.sparse.linalg
from numpy import cross, eye
from numpy.linalg import norm

def draw_grid(surface, width, height, grid_size=10, color=skia.ColorGRAY):
    paint = skia.Paint(Color=color, AntiAlias=True, Style=skia.Paint.kStroke_Style)
    canvas = surface.getCanvas()
    # Draw vertical lines
    for x in range(0, width, grid_size):
        canvas.drawLine(x, 0, x, height, paint)
    # Draw horizontal lines
    for y in range(0, height, grid_size):
        canvas.drawLine(0, y, width, y, paint)

def M(axis, theta):
    return scipy.sparse.linalg.expm(np.cross(np.eye(3), axis/norm(axis)*theta))

def update_image(*args):
    # Retrieve rotation angle and axis vector from the entry fields
    alpha_deg = float(angle_entry.get())
    alpha = np.radians(alpha_deg)
    vector = [float(entry.get()) for entry in vector_entries]

    different_approach = M(vector, alpha)
    single_list = [item for sublist in different_approach for item in sublist]

    matrix.setAll(*single_list)

    # Apply the transformation and update the image display
    apply_matrix_transform(input_image_path, temp_image_path, matrix)
    pil_image = Image.open(temp_image_path)
    photo = ImageTk.PhotoImage(pil_image)
    image_label.configure(image=photo)
    image_label.image = photo

def apply_matrix_transform(input_image_path, output_image_path, matrix):
    image = skia.Image.open(input_image_path)
    background_scalar = 2  # scale bg
    img_width = image.width() * background_scalar
    img_height = image.height() * background_scalar

    # Create a surface larger than the image for the grid background
    surface = skia.Surface(img_width, img_height)
    canvas = surface.getCanvas()

    # Clear the canvas with a black background
    canvas.clear(skia.ColorBLACK)

    # Draw the grid on the black background
    draw_grid(surface, img_width, img_height)

    # Apply the matrix transformation and draw the image on the grid
    canvas.save()  # Save the current canvas state
    canvas.concat(matrix)
    canvas.drawImage(image, 2*img_width//5, 2*img_height//5)
    canvas.restore()  # Restore the canvas to its state before the transformation was applied

    # Save the output image
    output_image = surface.makeImageSnapshot()
    output_image.save(output_image_path, skia.kPNG)

# Initialize tkinter
window = tk.Tk()
window.title("Matrix Presentatie Skia")
window.configure(bg='lightgrey')

# Labels for matrix entries
labels_text = [
    "scaleX", "skewX", "transX",
    "skewY", "scaleY", "transY",
    "persp0", "persp1", "persp2"
]

# Identity matrix
default_matrix_values = [1, 0, 0, 0, 1, 0, 0, 0, 1]

# Create entry fields and labels for the matrix values
entries = []
for i, text in enumerate(labels_text):
    row, col = divmod(i, 3)
    label = tk.Label(window, text=text, bg='lightgrey', fg='black')
    label.grid(row=row*2, column=col*2, sticky='e')
    entry = tk.Entry(window, bg="white", fg="black")
    entry.insert(0, str(default_matrix_values[i]))
    entry.grid(row=row*2, column=col*2+1)
    entries.append((label, entry))

# Add labels and entry fields for rotation angle and axis vector
angle_label = tk.Label(window, text="Rotation Angle (deg):", bg='lightgrey', fg='black')
angle_label.grid(row=8, column=0, sticky='e')
angle_entry = tk.Entry(window, bg="white", fg="black")
angle_entry.grid(row=8, column=1)

vector_labels = ["Axis X:", "Axis Y:", "Axis Z:"]
vector_entries = []
for i, label_text in enumerate(vector_labels):
    label = tk.Label(window, text=label_text, bg='lightgrey', fg='black')
    label.grid(row=9+i, column=0, sticky='e')
    entry = tk.Entry(window, bg="white", fg="black")
    entry.grid(row=9+i, column=1)
    vector_entries.append(entry)

input_image_path = 'input_plaatjes/charmander.png'
temp_image_path = 'output_plaatjes/charmander_temp.png'
matrix = skia.Matrix()

# Apply initial transformation (identity matrix), draw the grid, and display the image
apply_matrix_transform(input_image_path, temp_image_path, matrix)
pil_image = Image.open(temp_image_path)
photo = ImageTk.PhotoImage(pil_image)

size_label = tk.Label(window, text=f'size: {pil_image.size}', bg='lightgrey', fg='black')
size_label.grid(row=7, column=0, columnspan=1, pady=10)
image_label = tk.Label(window, image=photo, bg='black')
image_label.grid(row=6, column=0, columnspan=6, pady=10)

# Update image when Enter key is pressed in any entry field
for label, entry in entries:
    entry.bind('<Return>', update_image)

for entry in vector_entries:
    entry.bind('<Return>', update_image)

angle_entry.bind('<Return>', update_image)


window.mainloop()
