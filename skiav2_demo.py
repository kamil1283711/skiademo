import skia
import tkinter as tk
from PIL import Image, ImageTk

def draw_grid(surface, width, height, grid_size=10, color=skia.ColorGRAY):
    paint = skia.Paint(Color=color, AntiAlias=True, Style=skia.Paint.kStroke_Style)
    canvas = surface.getCanvas()
    # Draw vertical lines
    for x in range(0, width, grid_size):
        canvas.drawLine(x, 0, x, height, paint)
    # Draw horizontal lines
    for y in range(0, height, grid_size):
        canvas.drawLine(0, y, width, y, paint)

def apply_matrix_transform(input_image_path, output_image_path, matrix):
    image = skia.Image.open(input_image_path)
    background_scalar = 2  # scale bg
    img_width = image.width() * background_scalar
    img_height = image.height() * background_scalar

    # Create a surface larger than the image for the grid background
    surface = skia.Surface(img_width, img_height)
    canvas = surface.getCanvas()

    # Clear the canvas with a black background
    canvas.clear(skia.ColorBLACK)

    # Draw the grid on the black background
    draw_grid(surface, img_width, img_height)

    # Apply the matrix transformation and draw the image on the grid
    canvas.save()  # Save the current canvas state
    canvas.concat(matrix)
    canvas.drawImage(image, 0, 0)
    canvas.restore()  # Restore the canvas to its state before the transformation was applied

    # Save the output image
    output_image = surface.makeImageSnapshot()
    output_image.save(output_image_path, skia.kPNG)


def update_image(*args):
    # Retrieve matrix values from the entry fields for both matrices
    matrix_values1 = [float(entries1[i][1].get()) for i in range(9)]
    matrix_values2 = [float(entries2[i][1].get()) for i in range(9)]

    # Set the first matrix
    matrix1.setAll(*matrix_values1)
    # Apply the transformation for the first image
    apply_matrix_transform(input_image_path, temp_image_path1, matrix1)
    pil_image1 = Image.open(temp_image_path1)
    photo1 = ImageTk.PhotoImage(pil_image1)
    image_label1.configure(image=photo1)
    image_label1.image = photo1


    temp_matrix = skia.Matrix()
    temp_matrix.setAll(*matrix_values2)
    matrix2 = temp_matrix.Concat(temp_matrix,matrix1)


    # Apply the combined transformation for the second image
    apply_matrix_transform(input_image_path, temp_image_path2, matrix2)
    pil_image2 = Image.open(temp_image_path2)
    photo2 = ImageTk.PhotoImage(pil_image2)
    image_label2.configure(image=photo2)
    image_label2.image = photo2


# Initialize tkinter
window = tk.Tk()
window.title("Matrix Presentatie Skia")
window.configure(bg='lightgrey')

# Adjusted to create two sets of labels and entry fields
labels_text = [
    "scaleX", "skewX", "transX",
    "skewY", "scaleY", "transY",
    "persp0", "persp1", "persp2"
]
default_matrix_values = [1, 0, 0, 0, 1, 0, 0, 0, 1]

# First set of matrix controls
entries1 = []
for i, text in enumerate(labels_text):
    row, col = divmod(i, 3)
    label = tk.Label(window, text=text, bg='lightgrey', fg='black')
    label.grid(row=row, column=col*2, sticky='e')
    entry = tk.Entry(window, bg="white", fg="black")
    entry.insert(0, str(default_matrix_values[i]))
    entry.grid(row=row, column=col*2+1)
    entries1.append((label, entry))

# Second set of matrix controls, positioned to the right of the first set
entries2 = []
for i, text in enumerate(labels_text):
    row, col = divmod(i, 3)
    label = tk.Label(window, text=text, bg='lightgrey', fg='black')
    label.grid(row=row, column=col*2+6, sticky='e')  # Adjust grid placement
    entry = tk.Entry(window, bg="white", fg="black")
    entry.insert(0, str(default_matrix_values[i]))
    entry.grid(row=row, column=col*2+7)  # Adjust grid placement
    entries2.append((label, entry))

input_image_path = 'input_plaatjes/charmander.png'
temp_image_path1 = 'output_plaatjes/charmander_temp1.png'
temp_image_path2 = 'output_plaatjes/charmander_temp2.png'
matrix1 = skia.Matrix()

# Apply initial transformation (identity matrix), draw the grid, and display the first image
apply_matrix_transform(input_image_path, temp_image_path1, matrix1)
pil_image1 = Image.open(temp_image_path1)
photo1 = ImageTk.PhotoImage(pil_image1)
image_label1 = tk.Label(window, image=photo1, bg='black')
image_label1.grid(row=6, column=0, columnspan=6, pady=10)

# Placeholder for the second image, initially identical to the first
apply_matrix_transform(input_image_path, temp_image_path2, matrix1)
pil_image2 = Image.open(temp_image_path2)
photo2 = ImageTk.PhotoImage(pil_image2)
image_label2 = tk.Label(window, image=photo2, bg='black')
image_label2.grid(row=6, column=6, columnspan=6, pady=10)  # Adjust grid placement

# Update image when Enter key is pressed in any entry field
for _, entry in entries1 + entries2:
    entry.bind('<Return>', update_image)

window.mainloop()
